<?php

namespace Drupal\link_no_protocol\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'link' widget.
 *
 * @FieldWidget(
 *   id = "link_no_protocol",
 *   label = @Translation("Link No Protocol"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class NoProtocolLinkWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'remove_protocol_default_value' => TRUE,
      'add_www_automatically' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['remove_protocol_default_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove protocol from default value'),
      '#default_value' => $this->getSetting('remove_protocol_default_value'),
      '#description' => $this->t('Drupal add automatically the protocol to default values in Link fields. If checked, the protocol will be removed from the default value.'),
    ];

    $elements['add_www_automatically'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add www prefix automatically'),
      '#default_value' => $this->getSetting('add_www_automatically'),
      '#description' => $this->t('If checked, the www prefix will be added automatically to the URL.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Users are allowed to not use protocol in URLs.');
    $remove_protocol_default_value = $this->getSetting('remove_protocol_default_value');
    if ($remove_protocol_default_value) {
      $summary[] = $this->t('The protocol is removed from the default value');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getUserEnteredStringAsUri($string) {
    // By default, assume the entered string is an URI.
    $uri = trim($string);

    if (!preg_match('/^(http|https)/', $string)) {
      $temporary_uri = 'https://' . $uri;
      $parsed_url = parse_url($temporary_uri);

      if (!empty($parsed_url) && static::validateDomain($parsed_url['host'])) {
        $uri = $temporary_uri;
      }
    }

    // Process the default link widget behavior.
    $uri = parent::getUserEnteredStringAsUri($uri);

    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['uri'] = static::getUserEnteredStringAsUri($value['uri']);

      // Add www prefix automatically.
      $add_www_automatically = $this->getSetting('add_www_automatically');
      if ($add_www_automatically) {
        $parsed_url = parse_url($value['uri']);
        if (!empty($parsed_url['host']) && !preg_match('/^www\./', $parsed_url['host'])) {
          $parsed_url['host'] = 'www.' . $parsed_url['host'];
          $value['uri'] = $parsed_url['scheme'] . '://' . $parsed_url['host'] . $parsed_url['path'];
        }
      }

      $value += ['options' => []];
    }
    return $values;
  }

  /**
   * Function to validate a domain name.
   *
   * @param string $domain
   *   The domain name.
   *
   * @return bool
   *   Returns TRUE if the domain name is valid, FALSE otherwise.
   */
  public static function validateDomain($domain) {
    $valid = FALSE;
    if (filter_var(gethostbyname($domain), FILTER_VALIDATE_IP)) {
      $valid = TRUE;
    }
    return $valid;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // The HTML URL input doesn't allow you to contribute without the
    // protocol.
    $element['uri']['#type'] = 'textfield';

    $remove_protocol_default_value = $this->getSetting('remove_protocol_default_value');
    if ($remove_protocol_default_value && !empty($element['uri']['#default_value'])) {
      $url = str_replace(['http://', 'https://'], '', $element['uri']['#default_value']);
      $element['uri']['#default_value'] = $url;
    }

    return $element;
  }

}
