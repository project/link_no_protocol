# Link No Protocol Widget

The link_no_protocol module provides a widget that allows your users to not
contribute the protocol ( http(s)):// ) at the start of the URLs when they fill
a Link field.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/link_no_protocol).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/link_no_protocol).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The requirement is that you already have installed the module link from the
Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Select the Link No Protocol widget in your Form Display of your entity.


## Maintainers

- Philippe Joulot - [phjou](https://www.drupal.org/u/phjou)
